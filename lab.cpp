#include "stdafx.h"
#include <stdio.h>
#include "mpi.h"


int main(int  argc, char * argv[])
{
	int rank, resultlen, numb;
	char name[MPI_MAX_PROCESSOR_NAME];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(name, &resultlen);
	do {
		if (rank == 0) {
			printf("Enter a digit (0 or less to exit): ");
			fflush(stdout);
			scanf("%d", &numb);
		}
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Bcast(&numb, 1, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		printf("Process %d (%s) got %d\n", rank, name, numb);
		fflush(stdout);
	} while (numb> 0);
	MPI_Finalize();
	return 0;
}